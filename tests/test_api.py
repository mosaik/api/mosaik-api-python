import asyncio
import multiprocessing
import sys
from textwrap import dedent

import docopt
import pytest

import mosaik_api_v3
from example_sim.mosaik import ExampleSim
from example_sim.mosaik import main as examplesim_main
from mosaik_api_v3.connection import RemoteException, single_channel


def test_api():
    api = mosaik_api_v3.Simulator({"models": {"spam": {}}})
    assert api.meta == {
        "api_version": mosaik_api_v3.__api_version__,
        "models": {"spam": {}},
        "type": "time-based",
        "extra_methods": [],
    }
    assert api.init("sid", 1.0) == api.meta
    with pytest.raises(NotImplementedError):
        api.create(None, None)
    assert api.setup_done() is None
    with pytest.raises(NotImplementedError):
        api.step(None, None, None)
    with pytest.raises(NotImplementedError):
        api.get_data(None)


@pytest.mark.parametrize("error", [True, False])
def test_start_simulation(error, monkeypatch):
    async def mosaik():
        sim = ExampleSim()
        try:

            async def start_process():
                proc = multiprocessing.Process(target=examplesim_main)
                proc.start()
                return proc

            channel, proc = await asyncio.gather(
                single_channel("127.0.0.1", 5555),
                start_process(),
            )
            # Test receiving the welcome message:
            ret = await channel.send(["init", ["sid"], {"time_resolution": 1.0}])
            assert ret == sim.meta

            # Try extra method call
            ret = await channel.send(["example_method", [23], {}])
            assert ret == 23

            if not error:
                # Test sending a command:
                ret = await channel.send(["step", [0, {}, 0], {}])
                assert ret == 1
            else:
                # Force an error:
                with pytest.raises(RemoteException):
                    await channel.send(["foo", [], {}])
        except Exception as e:
            print(e)
        finally:
            await channel.close()
            await asyncio.sleep(0)
            proc.join()

    monkeypatch.setattr(sys, "argv", ["test", "-l", "debug", "127.0.0.1:5555"])
    asyncio.run(mosaik())


# @pytest.mark.skip
def test_start_simulation_connection_refused(capsys, monkeypatch):
    monkeypatch.setattr(sys, "argv", ["test", "127.0.0.1:5555"])
    var = examplesim_main()
    out, err = capsys.readouterr()
    assert not out
    assert var == (
        "INFO:mosaik_api_v3:Starting ExampleSim ...\n"
        "ERROR:mosaik_api_v3:Could not connect to mosaik.\n"
    )


@pytest.mark.parametrize(
    "err, args",
    [
        (docopt.DocoptExit, [""]),  # No arguments passed
        (1, ["", "spam"]),  # Wrong address format
    ],
)
def test_start_simulation_arg_errors(err, monkeypatch, args):
    """Wrong parameters passed on the command line."""
    monkeypatch.setattr(sys, "argv", args)
    sim = ExampleSim()
    if type(err) is int:
        ret = mosaik_api_v3.start_simulation(sim)
        assert ret == err
    else:
        with pytest.raises(err):
            mosaik_api_v3.start_simulation(sim)


def test_parse_args(monkeypatch):
    argv = ["spam", "-l", "debug", "--bar", "eggs", "localhost:1234"]
    desc = "Spam and eggs"
    extra_options = [
        "--foo       Enable foo",
        "--bar BAR   The bar parameter",
    ]
    monkeypatch.setattr(sys, "argv", argv)
    args = mosaik_api_v3._parse_args(desc, extra_options)
    assert args == {
        "HOST:PORT": "localhost:1234",
        "--log-level": "debug",
        "--remote": False,
        "--timeout": "60",
        "--foo": False,
        "--bar": "eggs",
    }


def test_parse_args_help(capsys, monkeypatch):
    argv = ["spam", "--help"]
    desc = "Spam and eggs"
    extra_options = [
        "--foo       Enable foo",
        "--bar BAR   The bar parameter",
    ]
    monkeypatch.setattr(sys, "argv", argv)
    with pytest.raises(SystemExit):
        mosaik_api_v3._parse_args(desc, extra_options)

    out, err = capsys.readouterr()
    print(out)
    assert out == dedent(
        """\
        Spam and eggs

        Usage:
            spam [options] HOST:PORT

        Options:
            HOST:PORT   Connect to this address
            -l LEVEL, --log-level LEVEL
                        Log level for simulator (trace, debug, info, warning, error, critical)
            -r, --remote
                        Simulator is to be started on a machine remote from mosaik
            -t TIME, --timeout TIME
                        Timeout in seconds for mosaik handshake [default: 60]
            --foo       Enable foo
            --bar BAR   The bar parameter
        """  # noqa: E501
    )
    assert err == ""


@pytest.mark.parametrize(
    ("addr", "expected"),
    [
        ("127.0.0.1:1234", ("127.0.0.1", 1234)),
        ("localhost:1234", ("127.0.0.1", 1234)),
        ("localhost:1234\r\n", ("127.0.0.1", 1234)),
        ("localhost", ValueError),
        ("127.0.0.1", ValueError),
        ("foobar:1234", ValueError),
    ],
)
def test_parse_addr(addr, expected):
    if isinstance(expected, tuple):
        ret = mosaik_api_v3._parse_addr(addr)
        assert ret == expected
    else:
        pytest.raises(expected, mosaik_api_v3._parse_addr, addr)
